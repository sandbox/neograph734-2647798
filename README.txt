- Google API Communicator module -
by Robin van Sloten (Neograph734)


-- Summary --

This module provides a connector for the google-php-api-client. It allows
depending modules to provide the required scopes and then authenticates the user
using those scopes.

This is an API module, which only need to be enabled if other modules depend on
it.


-- Requirements --

One of:
 * Composer Manager (preferred); https://www.drupal.org/project/composer_manager
 * Libraries API (alternative); https://www.drupal.org/project/libraries


-- Installation --

 * Install the module as usual, by uploading the module folder to the modules
 directory (DRUPAL_ROOT/sites/all/modules) and activate it from the Drupal
 modules page.

 * Composer Manager - If you installed this module via drush you should be good,
 otherwise see: https://www.drupal.org/node/2405805

 * Libraries API - Download the Google-api-php-client library from Github at
 this url: https://github.com/google/google-api-php-client/releases/latest. Then
 place it in the libraries folder (DRUPAL_ROOT/sites/all/libraries) so that to
 autoloader.php is reachable via
 DRUPAL_ROOT/sites/all/libraries/google-api-php-client/src/Google/autoload.php


-- Configuration --

This module comes with one settings page at
/admin/config/services/google_api_communicator. This page allows you to enter
the Google API credentials as provided by the Google Developers Console
(https://console.developers.google.com). After which an oAuth connection can be
established.

--- Google Developers Console ---

 * Create a new project if you have not already done so.
 * Go to the APIs page and enable the APIs required by the module.
 * Go to the Credentials page and create a new API Key for browsers.
 * Also create a new oAuth 2 key for web applications.
 * Enter the API key and the oAuth key on the module configuration page and then
 in the Google Developers Console update both keys with the correct url's as
 provided by this module's configuration page.
