<?php
/**
 * @file
 * API documentation for hooks provided by the Google API Communicator module.
 */

/**
 * Provides default scopes to the Google API Communicator module.
 *
 * Scopes defined in this hook will be added to all the Google API Communicator
 * authorization calls, so that all modules can provide the required scopes and
 * the oAuth authentication registers all required actions at once.
 *
 * @return array
 *   An array of scopes that should be loaded.
 */
function hook_google_api_communicator_scopes() {
  return array(
    'https://www.googleapis.com/auth/userinfo.email',
  );
}

/**
 * Acts after a user has authenticated with Google.
 *
 * @param int|null $uid
 *   The user id who authenticated.
 * @param \Google_Service_Oauth2_Userinfoplus $user_data
 *   User data as provided by Google.
 * @param array $token
 *   The OAuth token that was provided to the user.
 */
function hook_google_api_communicator_authenticated($uid, \Google_Service_Oauth2_Userinfoplus $user_data, array $token) {
}

/**
 * Acts after a user has revoked authentication with Google.
 *
 * @param int $uid
 *   The user id who revoked authentication.
 */
function hook_google_api_communicator_unauthenticated($uid) {
}
