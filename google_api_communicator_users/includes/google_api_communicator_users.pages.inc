<?php
/**
 * @file
 * Page callbacks for Google API Communicator Users.
 */

/**
 * Render users details.
 *
 * @param object|null $user
 *   The user to display, or NULL to display all users.
 *
 * @return array
 *   Render array for the page.
 *
 * @throws Exception
 */
function google_api_communicator_users_list($user = NULL) {
  $query = db_select('google_api_communicator_users', 'gu');
  $query->fields('gu', array('uid', 'mail', 'authenticated'));

  if ($user) {
    $query->condition('uid', $user->uid);
  }
  $table_rows = $query->execute()->fetchAllAssoc('uid', PDO::FETCH_ASSOC);

  $table_headers = array(
    t('User'),
    t('Email'),
    t('Status'),
    t('Actions'),
  );

  // Set the page limit.
  $limit = 50;
  $page = pager_default_initialize(count($table_rows), $limit, 0);
  $offset = $limit * $page;

  $links = array();

  foreach ($table_rows as $uid => &$row) {
    if ($row['authenticated'] === '1') {
      $links[] = array(
        'title' => t('Unauthenticate'),
        'href' => 'user/' . $uid . '/google_api_communicator/unauthenticate',
      );
    }
    else {
      $links[] = array(
        'title' => t('Authenticate'),
        'href' => 'user/' . $uid . '/google_api_communicator/authenticate',
      );
    }

    $row[] = theme('links__ctools_dropbutton',
      array(
        'links' => $links,
        '#attributes' => array(
          'class' => array('inline', 'links', 'actions', 'horizontal', 'right'),
        ),
      )
    );
  }

  $table = array(
    array(
      '#theme' => 'table',
      '#header' => $table_headers,
      '#rows' => array_slice($table_rows, $offset, $limit),
      '#caption' => 'Enabled',
    ),
    array(
      '#theme' => 'pager',
    ),
  );

  return array($table);
}

/**
 * Authentication page callback.
 */
function google_api_communicator_users_account_authenticate($user) {
  // Authenticate the user.
  google_api_communicator_authenticate($user->uid);
}

/**
 * Revoke access confirmation form.
 */
function google_api_communicator_users_account_revoke($form, &$form_state, $user) {
  $form_state['#uid'] = $user->uid;

  return confirm_form(
    $form,
    t('Are you sure you want to revoke access token of this account?'),
    'user/' . $user->uid . '/google_api_communicator',
    t("After revoking access, this account can no longer be used to make API calls anymore"),
    t('Revoke'),
    t('Cancel')
  );
}

/**
 * Implements hook_form_submit().
 */
function google_api_communicator_users_account_revoke_submit($form, &$form_state) {
  // Unauthenticate the user.
  $uid = $form_state['#uid'];
  google_api_communicator_unauthenticate($uid);
  $form_state['redirect'] = 'user/' . $uid . '/google_api_communicator';
}
