<?php
/**
 * @file
 * Administration page callbacks for Google API Communicator.
 */

/**
 * Page callback: Google API Communicator form.
 */
function google_api_communicator_form($form, &$form_state) {
  global $base_url;

  $form['auth'] = array(
    '#title' => t('Google API authentication details'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );
  $form['auth']['google_api_communicator_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#default_value' => variable_get('google_api_communicator_client_id'),
    '#size' => 64,
    '#maxlength' => 255,
    '#description' => t('Your Client ID as provided by the Google Developer Console.'),
    '#required' => TRUE,
  );
  $form['auth']['google_api_communicator_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client secret'),
    '#default_value' => variable_get('google_api_communicator_client_secret'),
    '#size' => 64,
    '#maxlength' => 255,
    '#description' => t('Your Client secret as provided by the Google Developer Console.'),
    '#required' => TRUE,
  );
  $form['auth']['google_api_communicator_developer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('google_api_communicator_developer_key'),
    '#size' => 64,
    '#maxlength' => 255,
    '#description' => t('The Browser key as defined in the Google Developer Console.'),
    '#required' => TRUE,
  );
  $form['auth']['base_uri'] = array(
    '#type' => 'item',
    '#title' => t('Base URL'),
    '#markup' => t("@base", array('@base' => $base_url)),
    '#description' => t('Enter this Base URL in the Google Developer Console. If your site is accessible with and without https and/or www, add all variations.'),
  );
  $form['auth']['redirect_uri'] = array(
    '#type' => 'item',
    '#title' => t('Redirect URI'),
    '#markup' => t("@red/google_api_communicator/response", array('@red' => $base_url)),
    '#description' => t('Enter this Redirect URI in the Google Developer Console. If your site is accessible with and without https and/or www, add all variations.'),
  );

  $form['actions']['submit_connect'] = array(
    '#type' => 'submit',
    '#value' => t('Connect with Google'),
    '#submit' => array('google_api_communicator_form_connect'),
  );

  $form['actions']['submit_disconnect'] = array(
    '#type' => 'submit',
    '#value' => t('Disconnect from Google'),
    '#submit' => array('google_api_communicator_form_disconnect'),
  );

  $access_token = variable_get('google_api_communicator_accesstoken');
  if (!empty($access_token)) {
    // We are already authenticated.
    $form['auth']['#title'] = variable_get('google_api_communicator_connected_email');
    $form['auth']['#collapsed'] = TRUE;
    $form['auth']['google_api_communicator_client_id']['#disabled'] = TRUE;
    $form['auth']['google_api_communicator_client_secret']['#disabled'] = TRUE;
    $form['auth']['google_api_communicator_developer_key']['#disabled'] = TRUE;
    $form['actions']['submit_connect']['#access'] = FALSE;
  }
  else {
    $form['actions']['submit_disconnect']['#access'] = FALSE;
  }

  return system_settings_form($form);
}

/**
 * Implements hook_form_submit().
 */
function google_api_communicator_form_connect($form, &$form_state) {
  // Save the settings.
  system_settings_form_submit($form, $form_state);
  // Authenticate the API.
  google_api_communicator_authenticate(NULL);
}

/**
 * Implements hook_form_submit().
 */
function google_api_communicator_form_disconnect($form, &$form_state) {
  // Save the settings.
  system_settings_form_submit($form, $form_state);
  // Unauthenticate the API.
  google_api_communicator_unauthenticate(NULL);
}
