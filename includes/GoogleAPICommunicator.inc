<?php
/**
 * @file
 * Contains the main Google API Communicator class.
 */

/**
 * Defines an exception thrown when something goes wrong.
 */
class GoogleAPICommunicatorException extends Exception {
}

/**
 * Class PicasaAPIAuthenticator handles connections and authentication.
 */
class GoogleAPICommunicator {

  /**
   * Google Client object.
   *
   * @var Google_Client $client
   *   Holds the Google Client object.
   */
  protected $client;

  /**
   * User id.
   *
   * @var int $uid
   *   Holds the Drupal user id.
   */
  protected $uid;

  /**
   * GoogleAPICommunicator constructor.
   *
   * @param int $uid
   *   The user id on which behalf to connect. Omit for global (site) user.
   *
   * @throws GoogleAPICommunicatorException
   *   Throws an error on invalid requests.
   */
  public function __construct($uid = NULL) {

    if (!empty($uid) && !module_exists('google_api_communicator_users')) {
      throw new GoogleAPICommunicatorException('Request on behalf of a user was received, but the Google API Communicator Users sub-module is not enabled.');
    }

    // Store the uid for future use within this object.
    $this->uid = $uid;

    // Check if the class is defined via composer manager, if not try to load
    // through libraries. If not available at all, error and stop.
    if (class_exists('Google_Client') || (module_exists('libraries') && ($library = libraries_load('google-api-php-client')) && $library['loaded'])) {
      // Set the scopes defined via hook_google_api_communicator_scopes().
      $scopes = implode(" ", google_api_communicator_get_scopes());

      try {
        // Initialize the library.
        $client = new Google_Client();
        $client->setApplicationName(check_plain(variable_get('site_name')));
        $client->setScopes($scopes);
        $client->setClientId(variable_get('google_api_communicator_client_id'));
        $client->setClientSecret(variable_get('google_api_communicator_client_secret'));
        $client->setRedirectUri(url('google_api_communicator/response', array('absolute' => TRUE)));
        $client->setDeveloperKey(variable_get('google_api_communicator_developer_key'));
        $client->setIncludeGrantedScopes(TRUE);
        $client->setAccessType('offline');

        $this->client = $client;

        // Do we already have an access token?
        $token = $this->getAccessToken();
        if (!empty($token)) {
          $client->setAccessToken($token);

          // Does the token need a refresh (and can we refresh)?
          if ($client->isAccessTokenExpired() && isset($token['refresh_token'])) {
            $client->refreshToken($token['refresh_token']);
            $this->setAccessToken($client->getAccessToken());
          }
          // Unable te refresh due to a missing refresh token?
          if ($client->isAccessTokenExpired() && !isset($token['refresh_token'])) {
            watchdog('Google API Communicator', 'Connection with Picasa was unexpectedly lost.', array(), WATCHDOG_ERROR);
            $this->unAuthenticate();
          }
        }
        // Return the client for use by other classes.
        return $this->client;
      }
      catch (Exception $e) {
        watchdog('Google API Communicator', 'Caught exception: @exception', array('@exception' => $e->getMessage()), WATCHDOG_ERROR);
      }
    }
    else {
      // There is no client, so throw an exception to stop further processing.
      throw new GoogleAPICommunicatorException(t('The google-api-php-client was required but could not be loaded.'));
    }
  }

  /**
   * Finishes the authentication; handles the response from Google.
   *
   * @param string $code
   *   Response code received from Google.
   */
  public function handleAuthentication($code) {
    $this->client->authenticate($code);

    // Obtain and save the token.
    $token = $this->client->getAccessToken();
    $this->setAccessToken($token);

    $google_oauth = new Google_Service_Oauth2($this->client);
    $data = $google_oauth->userinfo->get();

    // Invoke hooks.
    module_invoke_all('google_api_communicator_authenticated', $this->uid, $data, $token);
  }

  /**
   * Function to create an oAuth login link.
   */
  public function createAuthUrl() {
    if ($this->client) {
      $auth_url = $this->client->createAuthUrl();
      return $auth_url;
    }
    return FALSE;
  }

  /**
   * Function to unlink this module from an account.
   */
  public function unAuthenticate() {
    $this->client->revokeToken();
    $this->setAccessToken(array());
    module_invoke_all('google_api_communicator_unauthenticated', $this->uid);

    return TRUE;
  }

  /**
   * Returns the initialised Google_Client object.
   *
   * This might not be needed as the constructor also attempts to return a
   * Google_Client.
   *
   * @return Google_Client
   *   An initialized Google_Client object.
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * Getter for the access token.
   *
   * @return array|false
   *   An access token when available, or FALSE.
   */
  protected function getAccessToken() {
    if (empty($this->uid)) {
      $token = variable_get('google_api_communicator_accesstoken', FALSE);
    }
    else {
      $query = db_query('SELECT gu.token FROM {google_api_communicator_users} gu WHERE gu.uid = :uid', array(
        ':uid' => $this->uid,
      ));
      // Only read the first column of the first result.
      $token = $query->fetchColumn(0);
      if ($token) {
        $decrypted_token = decrypt($token);
        $token = (array) json_decode($decrypted_token);
      }
    }

    return $token;
  }

  /**
   * Setter for the access token.
   *
   * @param array $token
   *   The access token to store.
   */
  protected function setAccessToken(array $token) {
    if (empty($this->uid)) {
      variable_set('google_api_communicator_accesstoken', $token);
    }
    else {
      if (!empty($token)) {
        $token = encrypt(json_encode($token));
      }
      else {
        $token = '';
      }
      db_merge('google_api_communicator_users')
        ->key(array('uid' => $this->uid))
        ->updateFields(array(
          'token' => $token,
        ))
        ->execute();
    }
  }

}
